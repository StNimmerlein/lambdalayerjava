package de.andrena.aws.demo;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.List;

public class SorterLambda implements RequestHandler<List<String>, List<String>> {
    @Override
    public List<String> handleRequest(List<String> input, Context context) {
        return TastinessSorter.sort(input);
    }
}
