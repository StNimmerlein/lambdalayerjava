# AWS Lambda Layer Example For Java

## Structure

This project consists of two sub projects:
- ***Layer*** which builds a library (that provides a TastinessSorter class)
- ***Lambda*** which uses this library and creates an AWS Lambda function

The goal is to not bundle the library in the lambda jar but create a Layer with it for the Lambda.

## How to use

In the root directory execute `./gradlew buildZip` to compile both subprojects and bundle them in the required format:
- ***Layer*** will be packaged as a jar and then put into a zip file which can be found at `Layer/build/distributions`. This file can be uploaded as a Lambda Layer for Java.
- ***Lambda*** will be compiled and then be packed in a zip file which can be uploaded as a Java Lambda. Use the handler `de.andrena.aws.demo.SorterLambda::handleRequest`.

The dependencies of the Lambda project are not bundled in the zip file so the Lambda function will only work when the Layer is attached to it.

## What about the other requirements of the Lambda project?

The other dependency, `com.amazonaws:aws-lambda-java-core:1.2.1`, is not packaged in any of the two zip files. But since this is a AWS library, it will automatically be available in the Lambda runtime environment.

If you include other dependencies, they either need to be packaged in the Lambda zip or be provided by a Layer.