package de.andrena.aws.demo;

import java.util.List;
import java.util.stream.Collectors;

public class TastinessSorter {

    public static final String HOLY_FOOD = "Nudelauflauf";
    public static final String FOOD_FROM_HELL = "Pilze";

    public static List<String> sort(List<String> list) {
        return list.stream().sorted(TastinessSorter::compare).collect(Collectors.toList());
    }

    private static Integer compare(String food1, String food2) {
        if (food1.equals(food2)) {
            return 0;
        }
        if (HOLY_FOOD.equals(food1) || FOOD_FROM_HELL.equals(food2)) {
            return -1;
        }
        if (FOOD_FROM_HELL.equals(food1) || HOLY_FOOD.equals(food2)) {
            return 1;
        }
        return 0;
    }
}